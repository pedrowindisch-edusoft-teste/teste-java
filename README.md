# Relatório de execução

O teste levou cerca de 3.5h para ficar pronto. As minhas principais dificuldades foram:

* trabalhar com manipulação de JSON com gson, onde dúvidas foram sanadas pelo guia de uso da dependência.
* manter o código limpo e organizado, nomes de variáveis coerentes, etc.

Um dos requerimentos do teste era ter um jeito fácil de incluir novas formas de cálculo da média e
frequência de um aluno. Para adicionar novas formas, crie um classe implementando a interface 
ICalcMedia ou ICalcFrequencia e depois use o método adequado da classe Aluno, por exemplo:

```java
// utils/CalcMediaPonderadaUtil.java

public class CalcMediaPonderadaUtil implements ICalcMedia {
	public float calcular(float[] notas) {
		...calculo da média ponderada
	} 
}

// para adicionar essa forma:

CalcMediaPonderadaUtil calculoMediaPonderada = new CalcMediaPonderadaUtil();
Aluno aluno = new Aluno(...); 

aluno.setCalculoMedia(calculoMediaPonderada);
aluno.calcularMedia();

// ou também:

aluno.calcularMedia(calculoMediaPonderada);

```