package pedrowindisch.testejavaedusoft;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import pedrowindisch.testejavaedusoft.models.*;
import pedrowindisch.testejavaedusoft.requests.GravaResultadoRequest;
import pedrowindisch.testejavaedusoft.requests.RecuperaAlunosRequest;
import pedrowindisch.testejavaedusoft.utils.*;

public class Main {
	private static final ICalcFrequencia calculoFrequencia = new CalcFrequenciaUtil();
	private static final ICalcMedia calculoMedia = new CalcMediaAritmeticaUtil();
	private static final File relatorio = new File("relatorio.txt");
	
	// constantes
	private static final String CAMPO_RESULTADOS = "resultadoAluno";
	
	public static void main(String[] args) throws IOException {
		RecuperaAlunosRequest recuperaAlunosRequest = new RecuperaAlunosRequest();
		GravaResultadoRequest gravaResultadoRequest = new GravaResultadoRequest();
		
		JsonArray recuperaAlunosResposta = recuperaAlunosRequest.requisitar();
		
		Aluno[] alunos = AlunosUtil.criarArrayDeAlunos(recuperaAlunosResposta);
		
		JsonObject gravaResultadoBody = new JsonObject();
		gravaResultadoBody.add(CAMPO_RESULTADOS, new JsonArray()); 
		
		relatorio.createNewFile();
		FileWriter writer = new FileWriter(relatorio);
		
		for(Aluno aluno : alunos) {
			// Para mudar a forma de cálculo da frequência ou média,
			// crie uma nova class implementando a interface correspondente
			// e adicione a nova forma como abaixo
			aluno.setCalculoFrequencia(calculoFrequencia);
			aluno.setCalculoMedia(calculoMedia);
			
			aluno.calcularSituacaoFinal();
			aluno.adicionarDadosAoRelatorio(writer);
			
			gravaResultadoBody.get(CAMPO_RESULTADOS)
				.getAsJsonArray()
				.add(aluno.gerarJsonParaRequisicao());
		}
		
		writer.close();
		
		gravaResultadoRequest.requisitar(gravaResultadoBody);
	}
}
