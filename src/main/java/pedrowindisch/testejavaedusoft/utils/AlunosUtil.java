package pedrowindisch.testejavaedusoft.utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import pedrowindisch.testejavaedusoft.models.Aluno;

public class AlunosUtil {
	// Constantes da resposta da requisição
	private static final String CAMPO_TOTAL_AULAS = "TOAL_AULAS";
	private static final String CAMPO_NOME_ALUNO = "NOME";
	private static final String CAMPO_COD_ALUNO = "COD";
	private static final String CAMPO_FALTAS = "FALTAS";
	private static final String CAMPO_MATERIAS = "nota";
	private static final String CAMPO_NOTA = "NOTA";
	
	public static Aluno[] criarArrayDeAlunos(JsonArray respostaRequisicao) {
		int quantidadeDeAlunos = respostaRequisicao.size();
		Aluno[] alunos = new Aluno[quantidadeDeAlunos];
		
		for(int i = 0; i < quantidadeDeAlunos; i++) {
			JsonObject dadosAluno = (JsonObject) respostaRequisicao.get(i);
			
			int codigo = dadosAluno.get(CAMPO_COD_ALUNO).getAsInt();
			String nome = dadosAluno.get(CAMPO_NOME_ALUNO).getAsString();
			int totalAulas = dadosAluno.get(CAMPO_TOTAL_AULAS).getAsInt();
			
			JsonArray materias = dadosAluno.get(CAMPO_MATERIAS).getAsJsonArray();
			int quantidadeDeMaterias = materias.size();
			float[] notas = new float[quantidadeDeMaterias];
			int totalFaltas = 0;
			
			for(int j = 0; j < quantidadeDeMaterias; j++) {
				JsonObject materia = (JsonObject) materias.get(j);
				
				notas[j] = materia.get(CAMPO_NOTA).getAsFloat();
				totalFaltas += materia.get(CAMPO_FALTAS).getAsInt();
			}
			
			alunos[i] = new Aluno(codigo, nome, notas, totalAulas, totalFaltas);
		}
		
		return alunos;
	}
}
