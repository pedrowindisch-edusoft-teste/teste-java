package pedrowindisch.testejavaedusoft.utils;

import pedrowindisch.testejavaedusoft.models.ICalcMedia;

public class CalcMediaAritmeticaUtil implements ICalcMedia {
	public float calcular(float[] notas) {
		float somaNotas = 0;
			
		for(float nota : notas)
			somaNotas += nota;
			
		return somaNotas / notas.length;
	}
}