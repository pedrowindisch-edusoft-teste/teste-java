package pedrowindisch.testejavaedusoft.utils;

import pedrowindisch.testejavaedusoft.models.ICalcFrequencia;

public class CalcFrequenciaUtil implements ICalcFrequencia {
	public float calcular(int totalAulas, int totalFaltas) {
		return 100 - ((100 * totalFaltas) / totalAulas);
	}
}
