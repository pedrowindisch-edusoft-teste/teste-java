package pedrowindisch.testejavaedusoft.models;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Aluno {
	private static DecimalFormat mediaFormat = new DecimalFormat("#.00");
	
	private ICalcFrequencia calculoFrequencia;
	private SituacaoFinal situacaoFinal;
	private ICalcMedia calculoMedia;
	private double frequencia;
	private int totalFaltas;
	private int totalAulas;
	private float[] notas;
	private String nome;
	private float media;
	private int codigo;
	
	public Aluno(int codigo, String nome, float[] notas, int totalAulas, int totalFaltas) {
		this.totalFaltas = totalFaltas;
		this.totalAulas = totalAulas;
		this.codigo = codigo;
		this.notas = notas;
		this.nome = nome.replace("Aluno ", "");
	}
	
	public String getNome() {
		return this.nome;
	}
	
	public int getCodigo() {
		return this.codigo;
	}
	
	public float[] getNotas() {
		return this.notas;
	}
	
	public void setSituacaoFinal(SituacaoFinal situacao) {
		this.situacaoFinal = situacao;
	}
	
	public SituacaoFinal getSituacaoFinal() {
		return this.situacaoFinal;
	}
	
	public float getMedia() {
		return this.media;
	}
	
	public void setCalculoMedia(ICalcMedia calculoMedia) {
		this.calculoMedia = calculoMedia;
	}
	
	public void calcularMedia() {
		this.media = this.calculoMedia.calcular(this.notas);
	}
	
	public void calcularMedia(ICalcMedia calculoMedia) {
		this.media = calculoMedia.calcular(this.notas);
	}
	
	public double getFrequencia() {
		return this.frequencia;
	}
	
	public void setCalculoFrequencia(ICalcFrequencia calculoFrequencia) {
		this.calculoFrequencia = calculoFrequencia;
	}
	
	public void calcularFrequencia() {
		this.frequencia = this.calculoFrequencia.calcular(totalAulas, totalFaltas);
	}
	
	public void calcularFrequencia(ICalcFrequencia calculoFrequencia) {
		this.frequencia = calculoFrequencia.calcular(totalAulas, totalFaltas); 
	}
	
	public void calcularSituacaoFinal() {
		this.calcularFrequencia();
		this.calcularMedia();
		
		if(this.frequencia < 70) this.situacaoFinal = SituacaoFinal.RF;
		else if(this.media < 7) this.situacaoFinal = SituacaoFinal.RM;
		else this.situacaoFinal = SituacaoFinal.AP;
	}
	
	public void adicionarDadosAoRelatorio(FileWriter writer) throws IOException {
		writer.write("Nome: " + this.nome + "\n");

		writer.write("Notas: ");
		for(float nota : this.notas) {
			writer.append(nota + " ");
		}
		writer.write("\n");
		
		writer.write("Faltas: " + this.totalFaltas + "\n");
		writer.write("Média: " + String.format("%.2f", this.media) + "\n");
		writer.write("Resultado: " + this.situacaoFinal + "\n");
		writer.write("-".repeat(20) + "\n");
	}
	
	public JsonElement gerarJsonParaRequisicao() {
		JsonObject jsonAluno = new JsonObject();
		
		jsonAluno.addProperty("MEDIA", mediaFormat.format(this.media));
		jsonAluno.addProperty("RESULTADO", this.situacaoFinal.toString());
		jsonAluno.addProperty("COD_ALUNO", this.codigo);
		jsonAluno.addProperty("SEU_NOME", "Pedro Henrique Windisch");
		
		return jsonAluno;
	}
}
