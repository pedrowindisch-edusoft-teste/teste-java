package pedrowindisch.testejavaedusoft.models;

public interface ICalcFrequencia {
	public float calcular(int totalAulas, int totalFaltas);
}
