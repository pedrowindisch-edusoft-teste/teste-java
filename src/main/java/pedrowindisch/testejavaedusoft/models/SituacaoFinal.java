package pedrowindisch.testejavaedusoft.models;

public enum SituacaoFinal {
	AP("Aprovado"),
	RM("Reprovado por média"),
	RF("Reprovado por falta");
	
	public final String descricao;
	
	private SituacaoFinal(String descricao) {
		this.descricao = descricao;
	}
}