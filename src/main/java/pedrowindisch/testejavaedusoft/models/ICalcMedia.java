package pedrowindisch.testejavaedusoft.models;

public interface ICalcMedia {
	public float calcular(float[] notas);
}
