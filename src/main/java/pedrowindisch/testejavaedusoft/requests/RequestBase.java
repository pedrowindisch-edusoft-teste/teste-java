package pedrowindisch.testejavaedusoft.requests;

import java.io.IOException;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Request;

public class RequestBase {
	// api constants
	private final String URL_BASE = "http://desenvolvimento.edusoft.com.br/desenvolvimentoMentorWebG5/rest/servicoexterno";
	private final String ENDPOINT_EXECUTE = "/execute/";
	private final String ENDPOINT_TOKEN = "/token/";
	private final String API_USUARIO = "mentor";
	private final String API_SENHA = "123456";
	
	private String SERVICO_EXTERNO;
	private String API_TOKEN;
	
	// okhttp constants
	private final OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
	
	private void gerarNovoToken() throws IOException {
		Request novoTokenRequest = new Request.Builder()
				.url(URL_BASE + ENDPOINT_TOKEN + SERVICO_EXTERNO)
				.get()
				.addHeader("usuario", API_USUARIO)
				.addHeader("senha", API_SENHA)
				.build();
				
		Response response = okHttpClient.newCall(novoTokenRequest).execute();
			
		API_TOKEN = response.body().string();
	}
	
	protected JsonElement requisitar(RequestBody body) throws IOException {
		gerarNovoToken();
		
		Request dadosDosAlunosRequest = new Request.Builder()
			.url(URL_BASE + ENDPOINT_EXECUTE + SERVICO_EXTERNO)
			.post(body)
			.addHeader("Content-Type", "application/json")
			.addHeader("Token", API_TOKEN)
			.build();
		
		Response response = okHttpClient.newCall(dadosDosAlunosRequest).execute();
		String responseBody = response.body().string();
		
		return new JsonParser().parse(responseBody);
	}
	
	public RequestBase(String servicoExterno) {
		SERVICO_EXTERNO = servicoExterno;
	}
}
