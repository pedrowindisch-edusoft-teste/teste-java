package pedrowindisch.testejavaedusoft.requests;

import java.io.IOException;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import okhttp3.RequestBody;

public class RecuperaAlunosRequest extends RequestBase  {
	private static final String SERVICO_EXTERNO = "recuperaAlunos";
	private static final String CAMPO_DADOS_ALUNOS = "alunos";
	
	public RecuperaAlunosRequest() {
		super(SERVICO_EXTERNO);
	}
	
	public JsonArray requisitar() throws IOException {
		RequestBody body = RequestBody.create("", null);
		
		JsonElement resposta = super.requisitar(body);
		
		// A resposta da requisição é uma array com apenas um elemento dentro dela, um objeto,
		// então primeiro transformamos a resposta em uma array, pegamos seu único elemento
		// e o transformamos em um objeto.
		JsonObject responseAsObject = resposta.getAsJsonArray().get(0).getAsJsonObject();
		
		JsonArray dadosAlunos = responseAsObject.getAsJsonArray(CAMPO_DADOS_ALUNOS);
		
		return dadosAlunos;
	}
}
