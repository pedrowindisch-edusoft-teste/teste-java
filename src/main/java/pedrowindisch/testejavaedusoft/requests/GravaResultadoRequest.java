package pedrowindisch.testejavaedusoft.requests;

import java.io.IOException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class GravaResultadoRequest extends RequestBase {
	private final MediaType jsonMediaType = MediaType.parse("application/json; charset=utf-8");
	private static final String SERVICO_EXTERNO = "gravaResultado";
	
	public GravaResultadoRequest() {
		super(SERVICO_EXTERNO);
	}
	
	public JsonObject requisitar(JsonObject bodyJson) throws IOException {
		RequestBody body = RequestBody.create(bodyJson.toString(), jsonMediaType);
		
		JsonElement response = super.requisitar(body);
		
		return response.getAsJsonObject();
	}
}
